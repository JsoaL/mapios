//
//  HistorialController.swift
//  Mapios
//
//  Created by erik on 11/5/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit

class HistorialController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    

    var correo:String?
    @IBOutlet weak var labelCorreo: UILabel!
    @IBOutlet weak var labelOrigen: UILabel!
    @IBOutlet weak var labelDestino: UILabel!
    @IBOutlet weak var labelCuota: UILabel!
    @IBOutlet weak var labelAero: UILabel!
    @IBOutlet weak var labelPartida: UILabel!
    @IBOutlet weak var table: UITableView!
    var opciones:Array<Dictionary<String,String>>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelCorreo.text = correo!
        
        let url = URL(string: "http://microservice-noisy-fossa.mybluemix.net/user/allTravels")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: ["correo":correo!], options: .prettyPrinted)
        } catch { fatalError("\(error)") }
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
            } else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data {
                    do{
                        let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! Dictionary<String,Any>
                        self.opciones = jsonDic["viajes"] as? Array<Dictionary<String,String>>
                        self.table.reloadData()
                    }catch{}
                }
            }
        }
        task.resume()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = opciones {
            return array.count
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let array = opciones {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "celdaOpciones")
            let opcion = array[indexPath.row]
            cell.textLabel?.text = ("\(opcion["destino"]!)")
            cell.accessoryType = UITableViewCell.AccessoryType.detailButton
            return cell
        }
        else{
            return UITableViewCell(style: .default, reuseIdentifier: "celdaOpciones")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let array = opciones {
            let opcion = array[indexPath.row]
            labelAero.text = opcion["aeros"]!
            labelCuota.text = opcion["precio"]!
            labelOrigen.text = opcion["origen"]!
            labelDestino.text = opcion["destino"]!
            labelPartida.text = opcion["partida"]!
        }
    }

}
