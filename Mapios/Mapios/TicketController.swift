//
//  TicketController.swift
//  Mapios
//
//  Created by erik on 11/5/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit

class TicketController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var labelPrecio: UILabel!
    @IBOutlet weak var labelDeparture: UILabel!
    @IBOutlet weak var labelOri: UILabel!
    @IBOutlet weak var labelDest: UILabel!
    @IBOutlet weak var labelAero: UILabel!
    @IBOutlet weak var tabla: UITableView!
    
    var correo:String?
    var pais:String?
    var idOr:String?
    var idDes:String?
    var fecha:String?
    var opciones:Array<Dictionary<String,String>>?
    var opcion:Dictionary<String,String>?
    private var stBoard: UIStoryboard?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stBoard = UIStoryboard(name: "Main", bundle:nil)
        
        let url = URL(string: "http://microservice-noisy-fossa.mybluemix.net/user/getQuotesByPorts?pais=\(pais!)&origen=\(idOr!)&destino=\(idDes!)&date=\(fecha!)")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
            } else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data {
                    do{
                        
                        let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! Array<Dictionary<String,String>>
                        self.opciones = jsonDic
                        print(jsonDic)
                        self.tabla.reloadData()
                    }catch{}
                }
            }
        }
        task.resume()
    }
    
    @IBAction func eventVolar(_ sender: UIButton) {
        if let selected = opcion, let mail = correo{
            self.messageBox(messageTitle: "Confirmar", messageAlert: "Esta seguro que desea viajar a \(selected["destino"]!)?", messageBoxStyle: .alert, alertActionStyle: .default) {
                NotificationCenter.default.addObserver(self, selector: #selector(self.handler), name: NSNotification.Name(rawValue: "data"), object: nil)
                let requestObj = Request()
                requestObj.postRequest(method: "/user/addTravel", parameters: ["precio":selected["precio"]!,"origen":selected["origen"]!,"destino":selected["destino"]!,"aeros":selected["aeros"]!,"partida":selected["partida"]!,"correo":mail])
                
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = opciones {
            return array.count
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let array = opciones {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "celdaOpciones")
            let opcion = array[indexPath.row]
            cell.textLabel?.text = ("\(opcion["aeros"]!)")
            cell.accessoryType = UITableViewCell.AccessoryType.detailButton
            return cell
        }
        else{
            return UITableViewCell(style: .default, reuseIdentifier: "celdaOpciones")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let array = opciones {
            let opcion = array[indexPath.row]
            self.opcion = opcion
            labelPrecio.text = opcion["precio"]!
            labelDeparture.text = opcion["partida"]!
            labelOri.text = opcion["origen"]!
            labelDest.text = opcion["destino"]!
            labelAero.text = opcion["aeros"]!
        }
    }
    
    private func messageBox(messageTitle: String, messageAlert: String, messageBoxStyle: UIAlertController.Style, alertActionStyle: UIAlertAction.Style, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: messageTitle, message: messageAlert, preferredStyle: messageBoxStyle)
        let okAction = UIAlertAction(title: "Aceptar", style: alertActionStyle) { _ in
            completionHandler() // This will only get called after okay is tapped in the alert
        }
        let outAction = UIAlertAction(title: "Atras", style: alertActionStyle){ _ in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.addAction(outAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func handler(notification: Notification) {
        guard let data = notification.userInfo!["data"] else { return }
        DispatchQueue.main.async {
            let datos = data as! Dictionary<String,String>
            if let status = datos["status"] {
                if (status == "S"){
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    print("fallo")
                }
            }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "data"), object: nil)
        }
    }
    
}
