//
//  Request.swift
//  Mapios
//
//  Created by erik on 11/1/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import Foundation

class Request {
    private let api = "http://microservice-noisy-fossa.mybluemix.net"
    
    func postRequest(method: String, parameters: [String:Any]) {
        print("Params:\(parameters)")
        let urlApi = URL(string: "\(api)\(method)")!
        let session = URLSession.shared
        var request = URLRequest(url: urlApi)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch {
            fatalError("\(error)")
        }
        let thread = session.dataTask(with: request, completionHandler: execute)
        thread.resume()
    }
    
    func getRequest(url: String) {
        let session = URLSession.shared
        let urlApi = URL(string: url)!
        let request = URLRequest(url: urlApi)
        let thread = session.dataTask(with: request, completionHandler: executeGet)
        thread.resume()
    }
    
    private func execute(data : Data?, response : URLResponse?, error : Error?) {
        // Se valida si no hay error
        if(error == nil) {
            // Se obtienen los datos
            guard let datos = data else { return }
            // Se obtiene el json
            do {
                let jsonDic = try JSONSerialization.jsonObject(with: datos, options: .mutableContainers) as! Dictionary<String,Any>
                print("Response:\(jsonDic)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "data"), object: nil, userInfo: ["data": jsonDic])
            } catch {
                fatalError("Error al obtener la respuesta.\n\(error)")
            }
        }
    }
    
    private func executeGet(data : Data?, response : URLResponse?, error : Error?) {
        // Se valida si no hay error
        if(error == nil) {
            // Se obtienen los datos
            guard let datos = data else { return }
            // Se obtiene el json
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "data"), object: nil, userInfo: ["data": datos])
        }
    }
    
}
