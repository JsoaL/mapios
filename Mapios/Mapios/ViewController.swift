//
//  ViewController.swift
//  Mapios
//
//  Created by erik on 10/30/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit
import FanMenu
import Macaw
import Mapbox

class HidingLabel: UILabel {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if bounds.size.width < intrinsicContentSize.width {
            frame = CGRect.zero
        }
    }
}

class ViewController: UIViewController, MGLMapViewDelegate {
    
    var correo:String?
    private var stBoard: UIStoryboard?
    
    @IBOutlet weak var fanMenu: FanMenu!
    @IBOutlet weak var mapView: MGLMapView!
    @IBOutlet weak var labelLat1: UILabel!
    @IBOutlet weak var labelLon1: UILabel!
    @IBOutlet var uiView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stBoard = UIStoryboard(name: "Main", bundle:nil)
        
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.setCenter(CLLocationCoordinate2D(latitude: 19.43166440151765, longitude: -99.12802113668971), zoomLevel: 3, animated: true)
        //mapView.styleURL = MGLStyle.lightStyleURL
        //mapView.tintColor = .darkGray
        mapView.delegate = self
        
        let pointOne = MGLPointAnnotation()
        pointOne.coordinate = CLLocationCoordinate2D(latitude: 19.43166440151765, longitude: -99.12802113668971)
        pointOne.title = "Origen"
        mapView.addAnnotation(pointOne)
        
        let pointTwo = MGLPointAnnotation()
        pointTwo.coordinate = CLLocationCoordinate2D(latitude: 40.74072508698657, longitude: -73.98872628184779)
        pointTwo.title = "Destino"
        mapView.addAnnotation(pointTwo)
        
        // Add a single tap gesture recognizer. This gesture requires the built-in MGLMapView tap gestures (such as those for zoom and annotation selection) to fail.
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(sender:)))
        for recognizer in mapView.gestureRecognizers! where recognizer is UITapGestureRecognizer {
            singleTap.require(toFail: recognizer)
        }
        
        mapView.addGestureRecognizer(singleTap)
        fanMenu.menuRadius = 55.0
        fanMenu.duration = 0.2
        fanMenu.interval = (Double.pi + Double.pi/4, Double.pi + -4.5 * Double.pi/4)
        fanMenu.radius = 25.0
        
        fanMenu.button = FanMenuButton(
            id: "main",
            image: "menu",
            color: Color(val: 0xF44336)
        )
        fanMenu.items = [
            FanMenuButton(
                id: "idClima",
                image: "clima",
                color: Color(val: 0x673AB7)
            ),
            FanMenuButton(
                id: "idVuelos",
                image: "plane",
                color: Color(val: 0x4CAF50)
            ),
            FanMenuButton(
                id: "idHistorial",
                image: "history",
                color: Color(val: 0x009688)
            ),
            FanMenuButton(
                id: "idFood",
                image: "food",
                color: Color(val: 0xFF5722)
            )
        ]
        
        // call before animation
        fanMenu.onItemDidClick = { button in
            //print("ItemDidClick: \(button.id)")
            self.showView()
        }
        // call after animation
        fanMenu.onItemWillClick = { button in
            
            if(button.id == "idClima"){
                let resultViewController = self.stBoard!.instantiateViewController(withIdentifier: "viewClima") as! ClimaController
                resultViewController.lat = pointTwo.coordinate.latitude
                resultViewController.lon = pointTwo.coordinate.longitude
                resultViewController.place = self.labelLon1.text
                self.navigationController?.pushViewController(resultViewController, animated: true)
            }
            if(button.id == "idVuelos"){
                let resultViewController = self.stBoard!.instantiateViewController(withIdentifier: "viewAeros") as! AerosController
                resultViewController.latDes = pointTwo.coordinate.latitude
                resultViewController.lonDes = pointTwo.coordinate.longitude
                resultViewController.desti = self.labelLon1.text
                resultViewController.latOri = pointOne.coordinate.latitude
                resultViewController.lonOri = pointOne.coordinate.longitude
                resultViewController.origen = self.labelLat1.text
                resultViewController.correo = self.correo!
                self.navigationController?.pushViewController(resultViewController, animated: true)
            }
            if(button.id == "idFood"){
                let resultViewController = self.stBoard!.instantiateViewController(withIdentifier: "viewMeal") as! MealController
                resultViewController.latDes = pointTwo.coordinate.latitude
                resultViewController.lonDes = pointTwo.coordinate.longitude
                resultViewController.desti = self.labelLon1.text
                self.navigationController?.pushViewController(resultViewController, animated: true)
            }
            if(button.id == "idHistorial"){
                let resultViewController = self.stBoard!.instantiateViewController(withIdentifier: "viewHistorial") as! HistorialController
                resultViewController.correo = self.correo!
                self.navigationController?.pushViewController(resultViewController, animated: true)
            }
        }
        fanMenu.backgroundColor = .clear
        //print(correo!)
    }
    
    
    
    // MARK: - Feature interaction
    @objc @IBAction func handleMapTap(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            // Limit feature selection to just the following layer identifiers.
            let layerIdentifiers: Set = ["lighthouse-symbols", "lighthouse-circles"]
            
            // Try matching the exact point first.
            let point = sender.location(in: sender.view!)
            for feature in mapView.visibleFeatures(at: point, styleLayerIdentifiers: layerIdentifiers)
                where feature is MGLPointFeature {
                    guard let selectedFeature = feature as? MGLPointFeature else {
                        fatalError("Failed to cast selected feature as MGLPointFeature")
                    }
                    showCallout(feature: selectedFeature)
                    return
            }
            
            let touchCoordinate = mapView.convert(point, toCoordinateFrom: sender.view!)
            let touchLocation = CLLocation(latitude: touchCoordinate.latitude, longitude: touchCoordinate.longitude)
            
            // Otherwise, get all features within a rect the size of a touch (44x44).
            let touchRect = CGRect(origin: point, size: .zero).insetBy(dx: -22.0, dy: -22.0)
            let possibleFeatures = mapView.visibleFeatures(in: touchRect, styleLayerIdentifiers: Set(layerIdentifiers)).filter { $0 is MGLPointFeature }
            
            // Select the closest feature to the touch center.
            let closestFeatures = possibleFeatures.sorted(by: {
                return CLLocation(latitude: $0.coordinate.latitude, longitude: $0.coordinate.longitude).distance(from: touchLocation) < CLLocation(latitude: $1.coordinate.latitude, longitude: $1.coordinate.longitude).distance(from: touchLocation)
            })
            if let feature = closestFeatures.first {
                guard let closestFeature = feature as? MGLPointFeature else {
                    fatalError("Failed to cast selected feature as MGLPointFeature")
                }
                showCallout(feature: closestFeature)
                return
            }
            
            // If no features were found, deselect the selected annotation, if any.
            mapView.deselectAnnotation(mapView.selectedAnnotations.first, animated: true)
        }
    }
    
    func showCallout(feature: MGLPointFeature) {
        let point = MGLPointFeature()
        point.title = feature.attributes["category"] as? String
        point.coordinate = feature.coordinate
        mapView.selectAnnotation(point, animated: true, completionHandler: nil)
    }
    
    //yavsudyvsiysad
    
    // MARK: - MGLMapViewDelegate methods
    
    // This delegate method is where you tell the map to load a view for a specific annotation. To load a static MGLAnnotationImage, you would use `-mapView:imageForAnnotation:`.
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        
        if(annotation is MGLPointFeature){
            return MGLAnnotationView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        }else{
            if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "draggablePoint") {
                return annotationView
            } else {
                return DraggableAnnotationView(reuseIdentifier: "draggablePoint", size: 15,mapView: self.mapView, viewController: self)
            }
        }
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        
        if(annotation is MGLPointFeature)
        {
            
        }else{
            let camera = MGLMapCamera(lookingAtCenter: annotation.coordinate, fromDistance: 4500, pitch: 15, heading: 180)
            mapView.fly(to: camera, withDuration: 4, peakAltitude: 3000, completionHandler: nil)
        }
    }
    
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        
    }
    
    func mapView(_ mapView: MGLMapView, didDeselect annotation: MGLAnnotation) {
        print(annotation.coordinate)
        //mapView.removeAnnotations([annotation])
    }
    
    func showView() {
        self.view.bringSubviewToFront(uiView)
       /* self.view.bringSubviewToFront(fanMenu)
        let newValue: CGFloat = self.uiView.alpha == 0.0 ? 1.0 : 0.0
        UIView.animate(withDuration: 0.35, animations: {
            self.uiView.alpha = newValue
        })*/
    }

}

// MGLAnnotationView subclass
class DraggableAnnotationView: MGLAnnotationView {
    var mapa:MGLMapView?
    var viewC:ViewController?
    var title:String?
    
    init(reuseIdentifier: String, size: CGFloat, mapView: MGLMapView, viewController: ViewController) {
        super.init(reuseIdentifier: reuseIdentifier)
        mapa = mapView
        viewC = viewController
        // `isDraggable` is a property of MGLAnnotationView, disabled by default.
        isDraggable = true
        
        // This property prevents the annotation from changing size when the map is tilted.
        scalesWithViewingDistance = false
        
        // Begin setting up the view.
        frame = CGRect(x: 0, y: 0, width: size, height: size)
        
        backgroundColor = .darkGray
        
        // Use CALayer’s corner radius to turn this view into a circle.
        layer.cornerRadius = size / 2
        layer.borderWidth = 1
        layer.borderColor = UIColor.white.cgColor
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.1
    }
    
    // These two initializers are forced upon us by Swift.
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Custom handler for changes in the annotation’s drag state.
    override func setDragState(_ dragState: MGLAnnotationViewDragState, animated: Bool) {
        super.setDragState(dragState, animated: animated)
        
        switch dragState {
        case .starting:
            deleteLayersSources()
            print("Starting", terminator: "")
            startDragging()
        case .dragging:
            print(".", terminator: "")
        case .ending, .canceling:
            print("Ending")
            endDragging()
            addItemsToMap(lat: (self.annotation?.coordinate.latitude)!, log: (self.annotation?.coordinate.longitude)!)
            self.title = (self.annotation?.title!)!
            setLocation(lat: (self.annotation?.coordinate.latitude)!, log: (self.annotation?.coordinate.longitude)!)
        case .none:
            break
        @unknown default:
            fatalError("Unknown drag state")
        }
    }
    
    func setLocation(lat:Double, log:Double) {
        NotificationCenter.default.addObserver(self, selector: #selector(handlerLocation), name: NSNotification.Name(rawValue: "data"), object: nil)
        let requestObj = Request()
        requestObj.getRequest(url: "https://api.mapbox.com/geocoding/v5/mapbox.places/\(log)%2C\(lat).json?access_token=pk.eyJ1IjoianNvYWwiLCJhIjoiY2syZGl5cDB1MmFxZzNtb2pndzFzZWdicyJ9.m4pKuZbzk9TacdFCm4jvRQ&cachebuster=1572828604499&autocomplete=false&types=region&language=es")
    }
    
    @objc private func handlerLocation(notification: Notification) {
        guard let data = notification.userInfo!["data"] else { return }
        DispatchQueue.main.async {
            let datos = data as! Data
            do {
                let jsonDic = try JSONSerialization.jsonObject(with: datos, options: .mutableContainers) as! Dictionary<String,Any>
                let arry = jsonDic["features"]! as! Array<Any>
                let zero = arry.first! as! Dictionary<String,Any>
                if(self.title! == "Origen") {
                    self.viewC!.labelLat1.text = zero["place_name_es"]! as? String
                }
                if(self.title! == "Destino") {
                    self.viewC!.labelLon1.text = zero["place_name_es"]! as? String
                }
                
            } catch {
                print("GeoJSON parsing failed")
            }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "data"), object: nil)
        }
    }
    
    // When the user interacts with an annotation, animate opacity and scale changes.
    func startDragging() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.layer.opacity = 0.8
            self.transform = CGAffineTransform.identity.scaledBy(x: 1.5, y: 1.5)
        }, completion: nil)
        
        // Initialize haptic feedback generator and give the user a light thud.
        if #available(iOS 10.0, *) {
            let hapticFeedback = UIImpactFeedbackGenerator(style: .light)
            hapticFeedback.impactOccurred()
        }
    }
    
    func endDragging() {
        transform = CGAffineTransform.identity.scaledBy(x: 1.5, y: 1.5)
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.layer.opacity = 1
            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        }, completion: nil)
        
        // Give the user more haptic feedback when they drop the annotation.
        if #available(iOS 10.0, *) {
            let hapticFeedback = UIImpactFeedbackGenerator(style: .light)
            hapticFeedback.impactOccurred()
        }
    }
    
    func deleteLayersSources() {
        let arrayLayers = mapa!.style?.layers
        let arraySources = mapa!.style?.sources
        for i in 0...((arrayLayers?.count)!-1) {
            let layer = arrayLayers?[i]
            let idLayer = layer!.identifier
            
            if(idLayer == "lighthouse-circles") {
                mapa!.style?.removeLayer(layer!)
            }
            if(idLayer == "lighthouse-symbols") {
                mapa!.style?.removeLayer(layer!)
            }
        }
        for source in arraySources! {
            let idSource = (source.identifier)
            if(idSource == "us-lighthouses") {
                mapa!.style?.removeSource(source)
            }
        }
    }
    
    func addItemsToMap(lat:Double, log:Double) {
        // Parsing GeoJSON can be CPU intensive, do it on a background thread
        
        DispatchQueue.global(qos: .background).async(execute: {
            
            let url = URL(string: "https://api.mapbox.com/geocoding/v5/mapbox.places/\(log)%2C\(lat).json?access_token=pk.eyJ1IjoianNvYWwiLCJhIjoiY2syZGl5cDB1MmFxZzNtb2pndzFzZWdicyJ9.m4pKuZbzk9TacdFCm4jvRQ&cachebuster=1572767106808&autocomplete=false&types=poi&limit=10&language=es")!
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print("error: \(error)")
                } else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                    }
                    if let data = data, let _ = String(data: data, encoding: .utf8) {
                        
                        do {
                            guard let shapeCollectionFeature = try MGLShape(data: data, encoding: String.Encoding.utf8.rawValue) as? MGLShapeCollectionFeature else {
                                fatalError("Could not cast to specified MGLShapeCollectionFeature")
                            }
                            //print(shapeCollectionFeature.shapes)
                            let features = shapeCollectionFeature.shapes
                            //print(features)
                            
                            // MGLMapView.style is optional, so you must guard against it not being set.
                            guard let style = self.mapa!.style else { return }
                            
                            // You can add custom UIImages to the map style.
                            // These can be referenced by an MGLSymbolStyleLayer’s iconImage property.
                            //style.setImage(UIImage(named: "lighthouse")!, forName: "lighthouse")
                            
                            // Add the features to the map as a shape source.
                            let source = MGLShapeSource(identifier: "us-lighthouses", features: features, options: nil)
                            //style.removeSource(source)
                            style.addSource(source)
                            
                            let lighthouseColor = UIColor(red: 0.08, green: 0.44, blue: 0.96, alpha: 1.0)
                            
                            // Use MGLCircleStyleLayer to represent the points with simple circles.
                            // In this case, we can use style functions to gradually change properties between zoom level 2 and 7: the circle opacity from 50% to 100% and the circle radius from 2pt to 3pt.
                            
                            let circles = MGLCircleStyleLayer(identifier: "lighthouse-circles", source: source)
                            circles.circleColor = NSExpression(forConstantValue: lighthouseColor)
                            
                            // The circles should increase in opacity from 0.5 to 1 based on zoom level.
                            circles.circleOpacity = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)", [2: 0.5, 7: 1])
                            circles.circleRadius = NSExpression(format: "mgl_step:from:stops:($zoomLevel, 1, %@)", [2: 4, 7: 6])
                            
                            // Use MGLSymbolStyleLayer for more complex styling of points including custom icons and text rendering.
                            let symbols = MGLSymbolStyleLayer(identifier: "lighthouse-symbols", source: source)
                            symbols.iconImageName = NSExpression(forConstantValue: "lighthouse")
                            symbols.iconColor = NSExpression(forConstantValue: lighthouseColor)
                            symbols.iconScale = NSExpression(forConstantValue: 0.5)
                            symbols.iconHaloColor = NSExpression(forConstantValue: UIColor.white.withAlphaComponent(0.5))
                            symbols.iconHaloWidth = NSExpression(forConstantValue: 1)
                            symbols.iconOpacity = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)",
                                                               [5.9: 0, 6: 1])
                            
                            // "name" references the "name" key in an MGLPointFeature’s attributes dictionary.
                            symbols.text = NSExpression(forKeyPath: "name")
                            symbols.textColor = symbols.iconColor
                            symbols.textFontSize = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)",
                                                                [10: 10, 16: 16])
                            symbols.textTranslation = NSExpression(forConstantValue: NSValue(cgVector: CGVector(dx: 10, dy: 0)))
                            symbols.textOpacity = symbols.iconOpacity
                            symbols.textHaloColor = symbols.iconHaloColor
                            symbols.textHaloWidth = symbols.iconHaloWidth
                            symbols.textJustification = NSExpression(forConstantValue: NSValue(mglTextJustification: .left))
                            symbols.textAnchor = NSExpression(forConstantValue: NSValue(mglTextAnchor: .left))
                            //style.removeLayer(circles)
                            //style.removeLayer(symbols)
                            style.addLayer(circles)
                            style.addLayer(symbols)
                            
                        } catch {
                            print("GeoJSON parsing failed")
                        }
                    }
                }
            }
            task.resume()
        })
        
    }
}
