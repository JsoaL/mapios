//
//  LoginController.swift
//  Mapios
//
//  Created by erik on 10/30/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    private var stBoard: UIStoryboard?
    @IBOutlet weak var labelCorreo: UITextField!
    @IBOutlet weak var labelPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //NotificationCenter.default.addObserver(self, selector: #selector(handlerLogin), name: NSNotification.Name(rawValue: "data"), object: nil)
        stBoard = UIStoryboard(name: "Main", bundle:nil)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonRegistrar(_ sender: UIButton) {
        let resultViewController = stBoard!.instantiateViewController(withIdentifier: "viewRegistro") as! RegistroController
        self.navigationController?.pushViewController(resultViewController, animated: true)
    }
    
    @IBAction func buttonIniciar(_ sender: UIButton) {
        
        if let correo = labelCorreo.text {
            if let pass = labelPass.text {
                NotificationCenter.default.addObserver(self, selector: #selector(handlerLogin), name: NSNotification.Name(rawValue: "data"), object: nil)
                let requestObj = Request()
                requestObj.postRequest(method: "/user/login", parameters: ["correo":correo,"password":pass])
                
            }
        }
    }
    
    private func messageBox(messageTitle: String, messageAlert: String, messageBoxStyle: UIAlertController.Style, alertActionStyle: UIAlertAction.Style, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: messageTitle, message: messageAlert, preferredStyle: messageBoxStyle)
        let okAction = UIAlertAction(title: "Aceptar", style: alertActionStyle) { _ in
            completionHandler() // This will only get called after okay is tapped in the alert
        }
        let outAction = UIAlertAction(title: "Atras", style: alertActionStyle){ _ in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.addAction(outAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func handlerLogin(notification: Notification) {
        guard let data = notification.userInfo!["data"] else { return }
        DispatchQueue.main.async {
            let datos = data as! Dictionary<String,Any>
            if let nombre = datos["nombre"] {
                self.messageBox(messageTitle: "Confirmar", messageAlert: "Bienvenido \(nombre)", messageBoxStyle: .alert, alertActionStyle: .default) {
                    
                    let resultViewController = self.stBoard!.instantiateViewController(withIdentifier: "viewMapa") as! ViewController
                    resultViewController.correo = datos["correo"] as? String
                    self.navigationController?.pushViewController(resultViewController, animated: true)
                    
                }
            }
            if let status = datos["status"] {
                self.messageBox(messageTitle: "Datos incorrectos", messageAlert: "Intente de nuevo", messageBoxStyle: .alert, alertActionStyle: .default) { print(status)}
            }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "data"), object: nil)
        }
    }

}
