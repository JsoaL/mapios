//
//  RegistroController.swift
//  Mapios
//
//  Created by erik on 10/30/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit

class RegistroController: UIViewController {

    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var materno: UITextField!
    @IBOutlet weak var paterno: UITextField!
    @IBOutlet weak var correo: UITextField!
    @IBOutlet weak var pass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func eventRegistrarse(_ sender: UIButton) {
        
        if let correo = correo.text {
            if let pass = pass.text {
                if let nombre = nombre.text{
                    if let paterno = paterno.text {
                        if let materno = materno.text {
                            NotificationCenter.default.addObserver(self, selector: #selector(handler), name: NSNotification.Name(rawValue: "data"), object: nil)
                            let requestObj = Request()
                            requestObj.postRequest(method: "/user/create", parameters: ["nombre":nombre,"paterno":paterno,"materno":materno, "correo":correo,"password":pass])
                        }
                    }
                }
            }
        }
        
    }
    
    @objc private func handler(notification: Notification) {
        guard let data = notification.userInfo!["data"] else { return }
        DispatchQueue.main.async {
            let datos = data as! Dictionary<String,Any>
            if let status = datos["status"] as? String {
                if (status == "S") {
                    self.messageBox(messageTitle: "Exito", messageAlert: "Bienvenido \(self.nombre.text!)", messageBoxStyle: .alert, alertActionStyle: .default) {
                        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "data"), object: nil)
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
        }
    }
    
    private func messageBox(messageTitle: String, messageAlert: String, messageBoxStyle: UIAlertController.Style, alertActionStyle: UIAlertAction.Style, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: messageTitle, message: messageAlert, preferredStyle: messageBoxStyle)
        let okAction = UIAlertAction(title: "Aceptar", style: alertActionStyle) { _ in
            completionHandler() // This will only get called after okay is tapped in the alert
        }
        let outAction = UIAlertAction(title: "Atras", style: alertActionStyle){ _ in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.addAction(outAction)
        present(alert, animated: true, completion: nil)
    }

}
