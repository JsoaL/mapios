//
//  MealController.swift
//  Mapios
//
//  Created by erik on 11/5/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit

class MealController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var latDes: Double?
    var lonDes: Double?
    var desti: String?
    
    @IBOutlet weak var labelDest: UILabel!
    @IBOutlet weak var labelNombre: UILabel!
    @IBOutlet weak var labelComidas: UILabel!
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var tabla: UITableView!
    
    
    var opciones:Array<Dictionary<String,String>>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDest.text = desti
        
        let url = URL(string: "http://microservice-noisy-fossa.mybluemix.net/user/getRestaurants?lon=\(lonDes!)&lat=\(latDes!)")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
            } else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data {
                    do{
                        
                        let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! Array<Dictionary<String,String>>
                        self.opciones = jsonDic
                        self.tabla.reloadData()
                        print(jsonDic)
                    }catch{}
                }
            }
        }
        task.resume()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = opciones {
            return array.count
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let array = opciones {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "celdaOpciones")
            let opcion = array[indexPath.row]
            cell.textLabel?.text = ("\(opcion["nombre"]!)")
            cell.accessoryType = UITableViewCell.AccessoryType.detailButton
            return cell
        }
        else{
            return UITableViewCell(style: .default, reuseIdentifier: "celdaOpciones")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let array = opciones {
            let opcion = array[indexPath.row]
            labelNombre.text = opcion["nombre"]!
            labelComidas.text = opcion["comidas"]!
            labelRating.text = opcion["rating"]!
            //imagen.image = nil
            if let uerrele = opcion["imagen"] {
                if let url = URL(string: uerrele){
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url)
                        DispatchQueue.main.async {
                            self.imagen.image = UIImage(data: data!)
                        }
                    }
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
