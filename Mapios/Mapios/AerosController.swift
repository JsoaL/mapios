//
//  AerosController.swift
//  Mapios
//
//  Created by erik on 11/4/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit

class AerosController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var labelOrigen: UILabel!
    @IBOutlet weak var labelDestino: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var pickerView2: UIPickerView!
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var inputTextField: UITextField!
    private var stBoard: UIStoryboard?
    private var datePicker: UIDatePicker?
    
    var correo:String?
    var latOri:Double?
    var lonOri:Double?
    var origen:String?
    var latDes: Double?
    var lonDes: Double?
    var desti: String?
    var arrayOri: Array<Dictionary<String,String>>?
    var arrayDes: Array<Dictionary<String,String>>?
    
    var idPais:String?
    var idOri:String?
    var idDest:String?
    var date:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stBoard = UIStoryboard(name: "Main", bundle:nil)
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(AerosController.dateChanged(datePicker:)), for: .valueChanged)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AerosController.viewTapped(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        inputTextField.inputView = datePicker
        
        labelOrigen.text = origen!
        labelDestino.text = desti!
        
        let url = URL(string: "http://microservice-noisy-fossa.mybluemix.net/user/getAirPorts?lon=\(latOri!)&lat=\(lonOri!)")!
        print(url)
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
            } else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    do {
                        let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! Array<Dictionary<String,String>>
                        self.arrayOri = jsonDic
                        self.pickerView!.reloadAllComponents()
                        
                        let url = URL(string: "http://microservice-noisy-fossa.mybluemix.net/user/getAirPorts?lon=\(self.latDes!)&lat=\(self.lonDes!)")!
                        print(url)
                        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                            if let error = error {
                                print("error: \(error)")
                            } else {
                                if let response = response as? HTTPURLResponse {
                                    print("statusCode: \(response.statusCode)")
                                }
                                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                    do {
                                        let jsonDic = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! Array<Dictionary<String,String>>
                                        self.arrayDes = jsonDic
                                        self.pickerView2!.reloadAllComponents()
                                        
                                    } catch {
                                        print("GeoJSON parsing failed")
                                    }
                                    
                                }
                            }
                        }
                        task.resume()
                        
                        
                    } catch {
                        print("GeoJSON parsing failed")
                    }
                    
                }
            }
        }
        task.resume()
        
        /*
        NotificationCenter.default.addObserver(self, selector: #selector(handlerOri), name: NSNotification.Name(rawValue: "data"), object: nil)
        let requestObj = Request()
        requestObj.getRequest(url: "http://microservice-noisy-fossa.mybluemix.net/user/getAirPorts?lon=\(latOri!)&lat=\(lonOri!)")
    print("http://microservice-noisy-fossa.mybluemix.net/user/getAirPorts?lon=\(latOri!)&lat=\(lonOri!)")
        
        NotificationCenter.default.addObserver(self, selector: #selector(handlerDes), name: NSNotification.Name(rawValue: "data2"), object: nil)
        let requestObj2 = Request()
        requestObj2.getRequest(url: "http://microservice-noisy-fossa.mybluemix.net/user/getAirPorts?lon=\(latDes!)&lat=\(lonDes!)")
    print("http://microservice-noisy-fossa.mybluemix.net/user/getAirPorts?lon=\(latDes!)&lat=\(lonDes!)")*/
    }
    
    @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        inputTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @IBAction func eventContinuar(_ sender: UIButton) {
        
        if let pais = idPais, let idOr = idOri, let idDes = idDest, let fecha = inputTextField.text {
            /*print(pais)
            print(idOr)
            print(idDes)
            print(fecha)*/
            let resultViewController = self.stBoard!.instantiateViewController(withIdentifier: "viewTicket") as! TicketController
            resultViewController.pais = pais
            resultViewController.idOr = idOr
            resultViewController.idDes = idDes
            resultViewController.fecha = fecha
            resultViewController.correo = correo
            self.navigationController?.pushViewController(resultViewController, animated: true)
        }else{
            print("seleciona")
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.pickerView {
            if let arry = arrayOri {
                return (arry).count
            }else{
                return 1
            }
        } else if pickerView == pickerView2{
            if let arry = arrayDes
            {
                return (arry).count
            }else
            {
                return 1
            }
        }
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == self.pickerView {
            if let arry = arrayOri {
                return (arry)[row]["PlaceName"]
            }else{
                return "Cargando..."
            }
        } else if pickerView == pickerView2{
            
            if let arry = arrayDes
            {
                return (arry)[row]["PlaceName"]
            }else
            {
                return "Cargando.."
            }
        }
        return "1"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.pickerView {
            if let arry = arrayOri {
                let rowy = arry[row]
                self.idPais = String((rowy["CountryId"]!).split(separator: "-")[0])
                self.idOri = rowy["PlaceId"]
            }else{
                print("Cargando...")
            }
        } else if pickerView == pickerView2{
            if let arry = arrayDes
            {
                let rowy = arry[row]
                self.idDest = rowy["PlaceId"]
            }else
            {
                print("Cargando..")
            }
        }
    }
    /*
    @objc private func handlerOri(notification: Notification) {
        guard let data = notification.userInfo!["data"] else { return }
        DispatchQueue.main.async {
            let datos = data as! Data
            do {
                let jsonDic = try JSONSerialization.jsonObject(with: datos, options: .mutableContainers) as! Array<Dictionary<String,String>>
                print(jsonDic)
                self.arrayOri = jsonDic
                self.pickerView!.reloadAllComponents()
            } catch {
                print("GeoJSON parsing failed")
            }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "data"), object: nil)
        }
    }
    
    @objc private func handlerDes(notification: Notification) {
        guard let data = notification.userInfo!["data2"] else { return }
        DispatchQueue.main.async {
            let datos = data as! Data
            do {
                let jsonDic = try JSONSerialization.jsonObject(with: datos, options: .mutableContainers) as! Array<Dictionary<String,String>>
                print(jsonDic)
                self.arrayDes = jsonDic
                self.pickerView2!.reloadAllComponents()
            } catch {
                print("GeoJSON parsing failed")
            }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "data2"), object: nil)
        }
    }*/

}
