//
//  ClimaController.swift
//  Mapios
//
//  Created by erik on 11/4/19.
//  Copyright © 2019 informatica. All rights reserved.
//

import UIKit

class ClimaController: UIViewController {

    var lat:Double?
    var lon:Double?
    var place:String?
    
    @IBOutlet weak var labelLugar: UILabel!
    @IBOutlet weak var labelTemp: UILabel!
    @IBOutlet weak var labelMax: UILabel!
    @IBOutlet weak var labelMin: UILabel!
    @IBOutlet weak var iconClima: UIImageView!
    @IBOutlet weak var labelDes: UILabel!
    @IBOutlet weak var iconAtuendo: UIImageView!
    @IBOutlet weak var labelArriba: UILabel!
    @IBOutlet weak var labelAbajo: UILabel!
    @IBOutlet weak var labelCalza: UILabel!
    @IBOutlet weak var labelAcces: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.labelLugar.text = place!
        NotificationCenter.default.addObserver(self, selector: #selector(handlerLocation), name: NSNotification.Name(rawValue: "data"), object: nil)
        let requestObj = Request()
        requestObj.getRequest(url: "http://microservice-noisy-fossa.mybluemix.net/user/getForeClothes?lon=\(lon!)&lat=\(lat!)")

        // Do any additional setup after loading the view.
    }
    
    @objc private func handlerLocation(notification: Notification) {
        guard let data = notification.userInfo!["data"] else { return }
        DispatchQueue.main.async {
            let datos = data as! Data
            do {
                let jsonDic = try JSONSerialization.jsonObject(with: datos, options: .mutableContainers) as! Dictionary<String,Any>
                self.labelTemp.text = "\(String(Float(truncating: jsonDic["temperatura"] as! NSNumber)))º C"
                self.labelMax.text = "\(String(Float(truncating: jsonDic["max"] as! NSNumber)))º C"
                self.labelMin.text = "\(String(Float(truncating: jsonDic["min"] as! NSNumber)))º C"
                self.labelDes.text = jsonDic["descripcion"] as? String
                
                let atuendo = jsonDic["atuendo"] as! Dictionary<String,String>
                self.labelArriba.text = atuendo["arriba"]
                self.labelAbajo.text = atuendo["abajo"]
                self.labelCalza.text = atuendo["calza"]
                self.labelAcces.text = atuendo["accesos"]
                
                let url = URL(string: "http://openweathermap.org/img/wn/\(jsonDic["icono"] as! String)@2x.png")
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        self.iconClima.image = UIImage(data: data!)
                    }
                }
                
            } catch {
                print("GeoJSON parsing failed")
            }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "data"), object: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
